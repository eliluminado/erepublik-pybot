#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    This file is part of eRepublik PyBot.
#
#    eRepublik PyBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    eRepublik PyBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with eRepublik PyBot.  If not, see <http://www.gnu.org/licenses/>.

#    http://www.codigopython.com.ar <contacto@codigopython.com.ar>

"""Tools and functions necessary for development."""

import glob
import os
import subprocess
import tkinter as tk

from scss.compiler import compile_file

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
PATH_UI = CURRENT_DIR + "/erepublik_pybot/ui/"


def gen_ui(files_ui=None):
    """It generates interface files from the '.ui'."""
    pyuic5 = subprocess.Popen(["pyuic5 --version"], shell=True)
    pyuic5_return_code = pyuic5.wait()

    if not pyuic5_return_code:
        if files_ui is None:
            files_ui = glob.glob(PATH_UI + "*.ui")
        for f in files_ui:
            base_filename = os.path.basename(f)
            filename = os.path.splitext(base_filename)[0]
            command = subprocess.Popen(
                [
                    "pyuic5",
                    "--indent",
                    "4",
                    "--from-imports",
                    "-o",
                    PATH_UI + filename + ".py",
                    f
                ],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
            return_code = command.wait()
            if return_code:
                pass


def gen_doc():
    """Generate documentation and translations available."""
    pass


def gen_css(output_style="expanded"):
    """Compile Sass file to CSS code for Qt.

    Options for output_style:
        'nested', 'expanded', 'compact', 'compressed'
    """
    files_sccs = glob.glob(PATH_UI + "*.scss")
    for file_scss in files_sccs:
        css = compile_file(file_scss, output_style=output_style)
        path = os.path.splitext(file_scss)[0]
        dst = path + '.qss'
        with open(dst, 'w') as f:
            f.write(css)


class MainUI:

    """Main class for the GUI Interface."""

    # TODO: Convert SASS
    # TODO: Generate UI
    # TODO: Convert ".ui" individually or all
    # TODO: Generate backup files "ui" and put an extension ".bak" (optional)
    # TODO: Generate Docs
    # TODO: Add sphinx-apidoc Issue:#4
    # TODO: Support Internationalization in Doc Issue:#6
    # TODO: Support coverage in Doc (http://sphinx-doc.org/ext/coverage.html)
    def __init__(self, parent):
        """The variables are set and start the interface."""
        self.frame = tk.Frame(parent)
        self.frame.pack()

        self.ui = tk.IntVar(value=1)
        self.doc = tk.IntVar(value=1)
        self.css = tk.IntVar(value=1)
        self.setup_ui()

    def setup_ui(self):
        """Generate GUI widgets."""
        checkbox_gen_gui = tk.Checkbutton(
            self.frame,
            text="Generate GUI files.",
            variable=self.ui
        )
        checkbox_gen_gui.pack()

        checkbox_gen_doc = tk.Checkbutton(
            self.frame,
            text="Generate Doc.",
            variable=self.doc
        )
        checkbox_gen_doc.pack()

        checkbox_gen_css = tk.Checkbutton(
            self.frame,
            text="Compile Sass files.",
            variable=self.css
        )
        checkbox_gen_css.pack()

        run_button = tk.Button(self.frame, text="Run", command=self.run)
        run_button.pack()

    def run(self):
        """Execute selected actions."""
        if self.doc.get():
            gen_doc()
        if self.ui.get():
            gen_ui()
        if self.css.get():
            gen_css()

if __name__ == "__main__":
    root = tk.Tk()
    app = MainUI(root)

    root.mainloop()
