#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    This file is part of eRepublik PyBot.
#
#    eRepublik PyBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    eRepublik PyBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with eRepublik PyBot.  If not, see <http://www.gnu.org/licenses/>.

#    http://www.codigopython.com.ar <contacto@codigopython.com.ar>

"""Settings for generating documentation."""

import os
import sys
import datetime

sys.path.insert(0, os.path.abspath('..'))

import erepublik_pybot

# -- General configuration ------------------------------------------------

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.ifconfig',
]

templates_path = ['_templates']

source_suffix = '.rst'

source_encoding = 'utf-8-sig'

master_doc = 'index'

project = erepublik_pybot.__title__

author = erepublik_pybot.__author__

copyright = str(datetime.datetime.now().year) + author

version = erepublik_pybot.__version__
release = erepublik_pybot.__version__

language = 'es'

# -- Internationalization ----------------------------------------------

locale_dirs = ['_locale']
gettext_compact = False

exclude_patterns = []

pygments_style = 'sphinx'

todo_include_todos = True

# -- Options for HTML output ----------------------------------------------

on_rtd = os.environ.get('READTHEDOCS', None) == 'True'

if not on_rtd:  # only import and set the theme if we're building docs locally
    import sphinx_rtd_theme
    html_theme = 'sphinx_rtd_theme'
    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

htmlhelp_basename = 'eRepublikpyBotdoc'

intersphinx_mapping = {'https://docs.python.org/': None}
