#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    This file is part of eRepublik PyBot.
#
#    eRepublik PyBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    eRepublik PyBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with eRepublik PyBot.  If not, see <http://www.gnu.org/licenses/>.

#    http://www.codigopython.com.ar <contacto@codigopython.com.ar>

"""Installer for eRepublik pyBot."""

from setuptools import setup, find_packages

import erepublik_pybot

try:
    with open('requirements.txt') as f:
        REQUIREMENTS = f.read().splitlines()
except FileNotFoundError:
    REQUIREMENTS = []

with open('README.rst', encoding='utf-8') as f:
    DESCRIPTION = f.read()

setup(
    name=erepublik_pybot.__title__,
    version=erepublik_pybot.__version__,
    author=erepublik_pybot.__author__,
    author_email=erepublik_pybot.__contact__,
    url=erepublik_pybot.__homepage__,
    summary=erepublik_pybot.__summary__,
    # TODO: Develop "DESCRIPTION.rst" Issue #2
    description=erepublik_pybot.__summary__,
    long_description=DESCRIPTION,
    license=erepublik_pybot.__license__,
    keywords=erepublik_pybot.__keywords_space__,
    platforms=erepublik_pybot.__platforms__,
    home_page=erepublik_pybot.__homepage__,
    download_url=erepublik_pybot.__download_url__,
    project_utl=[
        ["Bug Tracker", erepublik_pybot.__bugtrack_url__],
        ["Documentation", erepublik_pybot.__docs_url__]
    ],
    requires_python=erepublik_pybot.__requires_python__,
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: Microsoft",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Topic :: Games/Entertainment"
    ],

    packages=find_packages(exclude=['docs', 'tests']),
    install_requires=REQUIREMENTS
)
