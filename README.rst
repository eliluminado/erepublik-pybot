eRepublik pyBot: Automates the task of the game
===============================================


.. image:: https://travis-ci.org/eliluminado/erepublik_pybot.svg
    :target: https://travis-ci.org/eliluminado/erepublik_pybot
    :alt: Travis

.. image:: https://img.shields.io/github/issues/eliluminado/erepublik_pybot.svg
    :target: https://github.com/eliluminado/erepublik_pybot/issues
    :alt: GitHub issues
    
.. image:: https://img.shields.io/github/forks/eliluminado/erepublik_pybot.svg
    :target: https://github.com/eliluminado/erepublik_pybot/network
    :alt: GitHub forks
    
.. image:: https://img.shields.io/github/stars/eliluminado/erepublik_pybot.svg
    :target: https://github.com/eliluminado/erepublik_pybot/stargazers
    :alt: GitHub stars

.. image:: https://img.shields.io/badge/license-GPLv3-blue.svg
    :target: https://raw.githubusercontent.com/eliluminado/erepublik_pybot/develop/LICENSE
    :alt: GitHub license

.. image:: https://img.shields.io/pypi/pyversions/eRepublik_pyBot.svg
    :target: https://pypi.python.org/pypi/eRepublik_pyBot/
    :alt: Python Versions

.. image:: https://img.shields.io/pypi/status/eRepublik_pyBot.svg
    :target: https://pypi.python.org/pypi/eRepublik_pyBot/
    :alt: Python Status

.. image:: https://img.shields.io/coveralls/eliluminado/erepublik_pybot.svg
    :target: https://coveralls.io/github/eliluminado/erepublik_pybot
    :alt: Coveralls

eRepublik pyBot is a tool that allows you to perform daily tasks of eRepublik, from work until fight. Available for Windows and Linux systems and totally free.

Features
--------

- Work
- Train
- Fight (In development)
- Make bulk deals (In development)
- Increase the level of training camps (In development)

Installation
------------


Documentation
-------------



Contribute
----------
