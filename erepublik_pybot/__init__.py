#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    This file is part of eRepublik PyBot.
#
#    eRepublik PyBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    eRepublik PyBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with eRepublik PyBot.  If not, see <http://www.gnu.org/licenses/>.

#    http://www.codigopython.com.ar <contacto@codigopython.com.ar>

"""Here is available the application metadata."""

__title__ = "eRepublik PyBot"
# FIXME: Improving description.
__summary__ = "Bot for the game eRepublik."
# FIXME: Create tags
__keywords__ = ["erepublik", "bot", "automation"]
__keywords_comma__ = ', '.join(__keywords__)
__keywords_space__ = ' '.join(__keywords__)
__version__ = "0.1.0"

__version_info__ = tuple([int(num) for num in __version__.split('.')])

__author__ = 'Alvarez Alejandro'
__contact__ = 'contacto@codigopython.com.ar'
__homepage__ = 'http://www.codigopython.com.ar'
__bugtrack_url__ = 'https://github.com/eliluminado/erepublik_pybot/issues'
# TODO: Create Changelog
__changelog__ = ""
__download_url__ = 'https://github.com/eliluminado/erepublik_pybot/releases'
__repository_url__ = 'https://github.com/eliluminado/erepublik_pybot/'
# TODO: Make Docs
__docs_url__ = ''
__docformat__ = 'restructuredtext'

__license__ = "GNU General Public License v3 (GPLv3)"
# TODO: Load LICENSE file
__license_text__ = "http://www.gnu.org/licenses/gpl-3.0.txt"
__license_url__ = "http://www.gnu.org/licenses/gpl-3.0-standalone.html"

__platforms__ = ["Windows", "Linux"]
__requires_python__ = [">=2.7"]
