#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    This file is part of eRepublik PyBot.
#
#    eRepublik PyBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    eRepublik PyBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with eRepublik PyBot.  If not, see <http://www.gnu.org/licenses/>.

#    http://www.codigopython.com.ar <contacto@codigopython.com.ar>

"""Sign sessions and send POST and GET requests."""

import re
from datetime import datetime
import time

import requests
from lxml import html
import simplejson as json

from db import database, Profile, Settings
from log import get_logger

logger_info = get_logger('events')
logger_debug = get_logger('debug')


def get_profile(user_id):
    """Extract basic data from a eRepublik profile."""
    # TODO: Integrating with Session.

    base_url = "http://www.erepublik.com/en/citizen/profile/"
    url = base_url + user_id
    r = requests.get(url)

    return r.content


class Session:

    """Handles HTTP requests made by the application.

    :ivar session: Stores the session in eRepublik
    :ivar session_status: Indicates whether currently connected
    :type session_status: bool
    :ivar url: Contains the address of the server
    :type url: str

    """

    def __init__(self, user_id=None, data_account=None):
        """Sign in eRepublik."""
        self.session = requests.Session()
        self.session_status = False
        self.token = None
        self.url = None
        self.offset_local = None
        self.offset_server = None

        self.user_id = user_id
        self.data_account = data_account

        logger_debug.info("Session is loaded")
        logger_debug.debug("user_id: " + str(self.user_id))
        logger_debug.debug("data_account: " + str(self.data_account))

        with open('table_xpath.json', 'r') as table_xpath_file:
            table_xpath = json.load(table_xpath_file)
            self.login_data_xpath = table_xpath['login_data']

    def login(self):
        """Login to eRepublik."""
        if self.user_id is not None:
            database.connect()
            user_profile = Profile.get(Profile.user_id == self.user_id)
            user_settings = Settings.get(Settings.user == self.user_id)

            citizen_email = user_profile.citizen_email
            citizen_password = user_profile.citizen_password
            protocol = user_settings.protocol
            host = user_settings.host
            user_agent = user_settings.user_agent
        elif (self.user_id is None) and (self.data_account is not None):
            citizen_email = self.data_account['citizen_email']
            citizen_password = self.data_account['citizen_password']
            protocol = self.data_account['protocol']
            host = self.data_account['host']
            user_agent = self.data_account['user_agent']
        else:
            pass

        self.url = protocol + host + '/en'

        login_url = self.url + '/login'

        # Space for headers
        self.session.headers['User-Agent'] = user_agent
        self.session.headers['Connection'] = 'keep-alive'
        self.session.headers['Accept-Encoding'] = 'gzip, deflate'
        self.session.headers['Host'] = host

        logger_info.info(self.session.headers['User-Agent'])
        logger_debug.info("Logging is started.")
        logger_debug.debug(self.session.headers)

        get_token = self.session.get(login_url)
        html_get_token = html.fromstring(get_token.content)
        self.token = html_get_token.xpath(self.login_data_xpath['token'])[0]

        params = {
            'citizen_email': citizen_email,
            'citizen_password': citizen_password,
            '_token': self.token,
            'remember': 'on'
        }

        # TODO: Handle timeout exception
        post = self.session.post(login_url, data=params)

        # FIXME: Choose a better name for the variable
        c = html.fromstring(post.content)
        logged_status = c.xpath(self.login_data_xpath["test"])
        if len(logged_status):
            if self.user_id is None:
                get_id = c.xpath(self.login_data_xpath["user_id"])[0]
                self.user_id = re.search("\d+$", get_id).group()
            self.session_status = True

            # Get offset timezone
            if time.localtime().tm_isdst == 0:
                offset_local = time.timezone
            else:
                offset_local = time.altzone
            self.offset_local = int(offset_local / 60 / 60 * -1)

            # FIXME: Fix year problem
            date_er = c.xpath(self.login_data_xpath["date"])[0]

            pattern = '\("live_time",(\d+),(\d+)\)'
            live_time = re.search(pattern, post.text)
            hour = live_time.group(1)
            minute = live_time.group(2)
            if len(minute) < 2:
                minute = '0' + minute
            time_er = hour + ':' + minute
            date_string = date_er + " - " + time_er
            h = datetime.strptime(date_string, "%b %d - %H:%M")
            h = h.replace(year=datetime.utcnow().year)
            offset_server = h - datetime.utcnow()
            self.offset_server = int(offset_server.total_seconds() / 60 / 60)

            return True
        else:
            pass

    def post(self, url, data=None, type_data=None):
        """Send POST requests."""
        if type_data == 'json':
            # TODO: Add to headers
            # Content-Type:application/x-www-form-urlencoded; charset=UTF-8
            pass

        request_post = self.session.post(self.url + url, data)
        return request_post

    def get(self, url, params=None):
        """Send GET requests."""
        # TODO: Manage exceptions
        request_get = self.session.get(self.url + url, params=params)
        return request_get

    def logout(self):
        """Close the active session."""
        self.session.cookies.clear()
        logger_debug.info("Closed session")
        self.session_status = False
        # FIXME: Fix closing connection
        """
        get_logout = self.session.get(self.url + '/logout')
        c = html.fromstring(get_logout.content)
        logged_status = c.xpath(self.login_data_xpath["test_out"])
        if len(logged_status) >= 1:
            logger_debug.info("Closed session")
            self.session_status = False
            return True
        else:
            pass
        """
