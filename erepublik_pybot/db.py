#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    This file is part of eRepublik PyBot.
#
#    eRepublik PyBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    eRepublik PyBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with eRepublik PyBot.  If not, see <http://www.gnu.org/licenses/>.

#    http://www.codigopython.com.ar <contacto@codigopython.com.ar>

"""Handles operations with the database."""

import peewee

database = peewee.SqliteDatabase('db_file.db')

# TODO: Use logging
# TODO: Encrypted database support Sqlcipher (http://goo.gl/sgIjM6)


class BaseModel(peewee.Model):

    """Base Model."""

    class Meta:
        database = database


class Profile(BaseModel):

    """Tables for profiles."""

    citizen_email = peewee.CharField(
        max_length=100,
        unique=True,
        null=False
    )

    citizen_password = peewee.CharField(
        max_length=50,
        null=False
    )

    user_id = peewee.CharField(
        max_length=8,
        null=False,
        unique=True
    )

    auto_start = peewee.BooleanField(default=True, null=True)

    last_login = peewee.DateField(null=True)

    class Meta:
        order_by = ('citizen_email',)


class Settings(BaseModel):

    """Tables for options."""

    # TODO: Support proxy.
    user = peewee.ForeignKeyField(Profile, to_field='user_id')

    language = peewee.CharField(
        max_length=5,
        null=True,
        default="en",
        # TODO: Generate list.
        choices=(
            ("en", "en"),
        )
    )

    protocol = peewee.CharField(
        max_length=8,
        choices=(
            ("https://", "https"),
            ("http://", "http")
        ),
        default="http://"
    )

    host = peewee.CharField(
        max_length=30,
        default="www.erepublik.com"
    )

    user_agent = peewee.CharField(
        max_length=100,
        default="Mozilla/5.0 (X11; Linux x86_64; rv:39.0)\
         Gecko/20100101 Firefox/39.0"
    )

    email_notification = peewee.BooleanField(default=False)

    popup_notification = peewee.BooleanField(default=False)

    debug_mode = peewee.BooleanField(default=True)
