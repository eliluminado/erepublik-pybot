#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    This file is part of eRepublik PyBot.
#
#    eRepublik PyBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    eRepublik PyBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with eRepublik PyBot.  If not, see <http://www.gnu.org/licenses/>.

#    http://www.codigopython.com.ar <contacto@codigopython.com.ar>

"""Establishes and configures the system logging."""

import logging
from logging.config import dictConfig

import simplejson as json
from simplejson import JSONDecodeError


# TODO: Enable use of debug mode
# TODO: Separate logs for users
# TODO: Send statistics to a remote server


def get_logger(name):
    """Create the logger."""
    try:
        with open('log_config.json', 'r') as f:
            log_config = json.load(f)
        # if debug_mode:
            # log_config["disable_existing_loggers"] = False
        dictConfig(log_config)

    except JSONDecodeError:
        pass
    except IOError:
        pass

    return logging.getLogger(name)
