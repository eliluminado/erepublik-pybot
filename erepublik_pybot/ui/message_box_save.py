# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/media/Principal/Alejandro/Proyectos/eRepublik_pyBot/erepublik_pybot/ui/message_box_save.ui'
#
# Created by: PyQt5 UI code generator 5.4.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets

class Ui_message_box_save(object):
    def setupUi(self, message_box_save):
        message_box_save.setObjectName("message_box_save")
        message_box_save.setWindowModality(QtCore.Qt.ApplicationModal)
        message_box_save.resize(400, 300)
        message_box_save.setModal(True)
        self.message_box_save_layout = QtWidgets.QVBoxLayout(message_box_save)
        self.message_box_save_layout.setObjectName("message_box_save_layout")
        self.progress_bar = QtWidgets.QProgressBar(message_box_save)
        self.progress_bar.setProperty("value", 0)
        self.progress_bar.setObjectName("progress_bar")
        self.message_box_save_layout.addWidget(self.progress_bar)
        self.list_actions_frame = QtWidgets.QFrame(message_box_save)
        self.list_actions_frame.setObjectName("list_actions_frame")
        self.list_actions_frame_2 = QtWidgets.QVBoxLayout(self.list_actions_frame)
        self.list_actions_frame_2.setObjectName("list_actions_frame_2")
        self.message_box_save_layout.addWidget(self.list_actions_frame)
        self.buttons_frame = QtWidgets.QFrame(message_box_save)
        self.buttons_frame.setObjectName("buttons_frame")
        self.buttons_frame_layout = QtWidgets.QHBoxLayout(self.buttons_frame)
        self.buttons_frame_layout.setObjectName("buttons_frame_layout")
        self.message_box_save_layout.addWidget(self.buttons_frame)

        self.retranslateUi(message_box_save)
        QtCore.QMetaObject.connectSlotsByName(message_box_save)

    def retranslateUi(self, message_box_save):
        _translate = QtCore.QCoreApplication.translate
        message_box_save.setWindowTitle(_translate("message_box_save", "Saving Settings"))

