# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/media/Principal/Alejandro/Proyectos/eRepublik_pyBot/erepublik_pybot/ui/ChooseUserUI.ui'
#
# Created by: PyQt5 UI code generator 5.4.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets

class Ui_ChooseUser(object):
    def setupUi(self, ChooseUser):
        ChooseUser.setObjectName("ChooseUser")
        ChooseUser.setWindowModality(QtCore.Qt.ApplicationModal)
        ChooseUser.resize(226, 54)
        self.main_layout = QtWidgets.QVBoxLayout(ChooseUser)
        self.main_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.setSpacing(0)
        self.main_layout.setObjectName("main_layout")
        self.header = QtWidgets.QFrame(ChooseUser)
        self.header.setObjectName("header")
        self.header_layout = QtWidgets.QVBoxLayout(self.header)
        self.header_layout.setContentsMargins(0, 0, 0, 0)
        self.header_layout.setSpacing(0)
        self.header_layout.setObjectName("header_layout")
        self.label_choose_user_intro = QtWidgets.QLabel(self.header)
        self.label_choose_user_intro.setAlignment(QtCore.Qt.AlignCenter)
        self.label_choose_user_intro.setObjectName("label_choose_user_intro")
        self.header_layout.addWidget(self.label_choose_user_intro)
        self.main_layout.addWidget(self.header)
        self.users_accounts_selectbox_layout = QtWidgets.QGridLayout()
        self.users_accounts_selectbox_layout.setObjectName("users_accounts_selectbox_layout")
        self.main_layout.addLayout(self.users_accounts_selectbox_layout)
        self.actions_buttons = QtWidgets.QFrame(ChooseUser)
        self.actions_buttons.setObjectName("actions_buttons")
        self.actions_buttons_layout = QtWidgets.QHBoxLayout(self.actions_buttons)
        self.actions_buttons_layout.setContentsMargins(0, 0, 0, 0)
        self.actions_buttons_layout.setSpacing(0)
        self.actions_buttons_layout.setObjectName("actions_buttons_layout")
        self.open_append_user = QtWidgets.QPushButton(self.actions_buttons)
        self.open_append_user.setObjectName("open_append_user")
        self.actions_buttons_layout.addWidget(self.open_append_user)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.actions_buttons_layout.addItem(spacerItem)
        self.close_button = QtWidgets.QPushButton(self.actions_buttons)
        self.close_button.setObjectName("close_button")
        self.actions_buttons_layout.addWidget(self.close_button)
        self.main_layout.addWidget(self.actions_buttons)

        self.retranslateUi(ChooseUser)
        QtCore.QMetaObject.connectSlotsByName(ChooseUser)

    def retranslateUi(self, ChooseUser):
        _translate = QtCore.QCoreApplication.translate
        ChooseUser.setWindowTitle(_translate("ChooseUser", "Choose an User"))
        self.label_choose_user_intro.setText(_translate("ChooseUser", "Choose an user:"))
        self.open_append_user.setText(_translate("ChooseUser", "Add Account"))
        self.close_button.setText(_translate("ChooseUser", "Close"))

