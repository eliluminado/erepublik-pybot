#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    This file is part of eRepublik PyBot.
#
#    eRepublik PyBot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    eRepublik PyBot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with eRepublik PyBot.  If not, see <http://www.gnu.org/licenses/>.

#    http://www.codigopython.com.ar <contacto@codigopython.com.ar>

"""Sets the available languages."""

import gettext
import os

TRANSLATION_DOMAIN = "eRepublik_pybot"

langs_path = os.path.dirname(os.path.abspath(__file__)) + "/langs"
os.listdir(langs_path)
availables_langs = os.listdir(langs_path)


def lang(language="en"):
    """It is discarded to implement multilanguage support in Qt."""
    gettext.install(True)
    gettext.bindtextdomain(
        TRANSLATION_DOMAIN,
        'langs'
    )
    gettext.textdomain(TRANSLATION_DOMAIN)
    return gettext.translation(
        domain=TRANSLATION_DOMAIN,
        localedir='langs',
        languages=[language],
        fallback=True
    )
